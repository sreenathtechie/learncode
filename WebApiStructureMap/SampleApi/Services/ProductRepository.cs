﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SampleApi.Services
{
    public class ProductRepository : IProductRepository
    {
        public ProductRepository()
        {
            File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + @"\logerror.txt",
                                    "ProductRepository constrcutor" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + Environment.NewLine);
        }

        public string Get(int id)
        {
            return "Product " + id;
        }

        public IEnumerable<string> GetAll()
        {
            return new List<string>()
            {
                "Product 1",
                "Product 2",
                "Product 3"
            };
        }
    }
}