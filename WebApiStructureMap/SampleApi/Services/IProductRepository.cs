﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleApi.Services
{
    public interface IProductRepository
    {
        IEnumerable<string> GetAll();
        string Get(int id);
    }
}
